﻿using System;
using System.Collections.Generic;

namespace Calculator
{
    public class Calculator
    {
        private static int GetActionPriority(char symbol)
        {
            switch (symbol)
            {
                case '(': return 0;
                case ')': return 1;
                case '+': return 2;
                case '-': return 3;
                case '*': return 4;
                case '/': return 4;
                default: return 5;
            }
        }

        private static bool IsDelimeter(char symbol)
        {
            return " ".IndexOf(symbol) != -1;
        }

        private static bool IsAction(char symbol)
        {
            return "+-/*()".IndexOf(symbol) != -1;
        }


        public bool CheckInput(string input)
        {
            foreach (var symbol in input)
            {
                if (IsDelimeter(symbol))
                {
                    continue;
                }

                if (IsAction(symbol))
                {
                    continue;
                }

                if (char.IsDigit(symbol))
                {
                    continue;
                }

                return false;
            }

            return true;
        }

        private static string GetExpression(string input)
        {
            var output = string.Empty; 

            var operators = new Stack<char>(); 

            for (var i = 0; i < input.Length; i++) 
            {
                
                if (IsDelimeter(input[i]))
                    continue; // calc can work with spaces. Just forget them

                
                if (char.IsDigit(input[i])) //here take a full number from string.
                {
                   
                    while (!IsDelimeter(input[i]) && !IsAction(input[i]))
                    {
                        output += input[i]; 
                        i++;

                        if (i == input.Length) break; 
                    }

                    output += " "; 
                    i--; 
                }

                
                if (!IsAction(input[i])) continue;
                if (input[i] == '(') //searching for ( and expression bettween )
                    operators.Push(input[i]); 
                else if (input[i] == ')') 
                {
                    
                    var s = operators.Pop();

                    while (s != '(')
                    {
                        output += s.ToString() + ' ';
                        s = operators.Pop();
                    }
                }
                else //searching other operatiors
                {
                    if (operators.Count > 0) 
                        if (GetActionPriority(input[i]) <= GetActionPriority(operators.Peek())) 
                            output += operators.Pop() + " "; //if operator is top in stack adding it to postfix output

                    operators.Push(char.Parse(input[i].ToString())); //if none, adding it to stack

                }
            }

            //add operators
            while (operators.Count > 0)
                output += operators.Pop() + " ";

            return output; 
        }


        private static double Work(string input)
        {
            double result = 0; 
            var temp = new Stack<double>(); 

            for (var i = 0; i < input.Length; i++) 
            {
                //take a full number from string
                if (char.IsDigit(input[i]))
                {
                    var fullNumber = string.Empty;

                    while (!IsDelimeter(input[i]) && !IsAction(input[i])) 
                    {
                        fullNumber += input[i];
                        i++;
                        if (i == input.Length) break;
                    }
                    temp.Push(double.Parse(fullNumber));
                    i--;
                }
                else if (IsAction(input[i])) //calculating expressions
                {
                   //take last 2 nubers from stack and calculationg them
                    var first = temp.Pop();
                    var second = temp.Pop();

                    switch (input[i]) 
                    {
                        case '+': result = second + first; break;
                        case '-': result = second - first; break;
                        case '*': result = second * first; break;
                        case '/': result = second / first; break;
                    }
                    temp.Push(result); //moving result back to stack
                }
            }
            return temp.Peek(); 
        }

        public double Calc(string input)
        {
            var output = GetExpression(input); //first making a postfix expression
            var result = Work(output); //calculating that expression
            return result; 
        }
    }
}
