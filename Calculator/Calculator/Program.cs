﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculator = new Calculator();
            while (true) 
            {
                Console.WriteLine("Plz input an expression: ");
                var input = Console.ReadLine();
                if (!calculator.CheckInput(input))
                {
                    Console.WriteLine("Wrong Input.");
                }
                else
                {
                    Console.WriteLine(calculator.Calc(input));
                }
            }
        }
    }
}
